package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import app.properties.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
	FileStorageProperties.class
})

public class UploadFileRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UploadFileRestApplication.class, args);
	}
}
