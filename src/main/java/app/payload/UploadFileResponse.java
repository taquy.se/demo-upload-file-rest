package app.payload;

public class UploadFileResponse {

	private String name;
	private String download;
	private String url;
	private String type;
	private long size;

	public UploadFileResponse(String name, String download, String url, String type, long size) {
		super();
		this.name = name;
		this.download = download;
		this.url = url;
		this.type = type;
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDownload() {
		return download;
	}

	public void setDownload(String download) {
		this.download = download;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

}
