package app.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import app.payload.UploadFileResponse;
import app.services.FileStorageService;

@RestController
public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Autowired
	private FileStorageService fss;

	@PostMapping("/upload")
	public UploadFileResponse upload(@RequestParam("file") MultipartFile file) {
		String fn = fss.store(file);

		String downloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/download/").path(fn)
				.toUriString();

		return new UploadFileResponse(fn, downloadUri, "", file.getContentType(), file.getSize());
	}
	// View images
	@RequestMapping(value = "/{fn}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getImage(@PathVariable String fn, HttpServletResponse response) throws IOException {

		Resource resource = fss.load(fn);

		byte[] bytes = StreamUtils.copyToByteArray(resource.getInputStream());

		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(bytes);
	}

	@PostMapping("/upload/multiple")
	public List<UploadFileResponse> uploadMulti(@RequestParam("files") MultipartFile[] files) {
		List<UploadFileResponse> results = Arrays.asList(files).stream().map(file -> {
			return upload(file);
		}).collect(Collectors.toList());
		return results;
	}

	@GetMapping("/download/{fn:.+}")
	public ResponseEntity<Resource> download(@PathVariable String fn, HttpServletRequest rq)
			throws FileNotFoundException {
		Resource resource = fss.load(fn);

		String contentType = null;
		try {
			contentType = rq.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("Could not determine file type.");
		}

		if (contentType == null)
			contentType = "application/octet-stream";

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}
}